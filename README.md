The script, hyperParameterTesting.py can be run to identify the optimum set of parameters to use for a given set of images. 

If you want to downsample the images or extract images from a video to process, see the helperFunctions for relevant functions and just change the directories and scales for your needs. 

To tune parameters:
* Create a directory of images containing the target data
* SPECIFY where the images are and ensure the correct imagae prefix is used (ie jpg, png...)
* Specify what parameters you want to select for tuning. See the code for details on how to either use the default, specify a value, specify a range or allow for automatic generation of a range of parameters
* OPTIONAL, specify the IDs to be identified. 

