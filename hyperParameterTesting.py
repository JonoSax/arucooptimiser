'''
this script is implementing as many of the aruco functions as possible
to just understand what it is doing.

NOTE this needs to be translated into C++
'''

from time import time
import cv2.aruco as aruco
from glob import glob
import cv2
import numpy as np
from itertools import product
import multiprocessing
import ray
ray.init()

class paramCreate:

    '''
    Object to store the combination of parameters and their magnitues along
    with the results of the aruco detection
    '''

    def __init__(self, param = [], paramValue = [], markerIDs = [], paramDict = None, time = np.inf):

        '''
        params:         list of parameters to change
        paramValue:     the parameter values used for the parameter dictionary
        markerIDs:      the aruco IDs which are being identified
        paramDict:      pre-existing parameter dictionary
        time:           the time per frame taken to evaluate using these parameters
        '''

        self.param = param
        self.values = paramValue

        # create a dictionary to store the results in using the markerIDs
        valueCount = {}
        if markerIDs is not None:
            for m, _ in enumerate(markerIDs):
                valueCount[m] = 0
        valueCount["other"] = 0
        valueCount["imgCount"] = 0
        self.time = time
        self.markerCount = valueCount
        self.paramDict = self.createParameters(paramDict)

    def createParameters(self, paramDict = None):

        '''
        With a list of parameter keys and associated parameter values, 
        assign these as the new parameter list to use. Any keys not set
        will be the default values.

            Inputs:
        paramDict:      if there is a previously used paramDict, change those values, 
        otherwise initialise the default parameter dictionary

            Outputs:
        paraDict:       parameter dictionary with update parameters
        '''

        # if no specific parameter dictionary specified, build from the 
        # default one
        if paramDict is None:
            paramDict = aruco.DetectorParameters_create()
        for pK, pV in zip(self.param, self.values):
            try:
                # get the type of variable 
                pT = type(eval(f"paramDict.{pK}"))
                if pV is None:
                    continue
                elif pT == int:
                    exec(f"paramDict.{pK} = int({pV})")
                elif pT == float:
                    exec(f"paramDict.{pK} = float({pV})")
            except:
                print(f"{pK} failed with {pV}")
        return(paramDict)

    def results(self, ids):

        '''
        Take the raw ID values and accumulate each value
        '''

        if ids is not None:
            for i in ids:
                i = i[0]
                if self.markerCount.get(i, None) is None:
                    self.markerCount["other"] += 1
                else:
                    self.markerCount[i] += 1

            self.markerCount["imgCount"] += 1

    # have some class function which calculates the true ppositive/false positive etc.

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()

def createArucoDictionary(selectKeys = None):

    '''
    Creating an dictionary which specified the aruco settings 
    which can be used to determine the aruco processes

    Choose the default dictionary from the list below (as taken 
    from https://docs.opencv.org/master/d9/d6a/group__aruco.html#gac84398a9ed9dd01306592dd616c2c975)
    or create a custom dictionary 

        Inputs
    selectKeys:     list of aruco IDs which are specifically being searched for

        Outputs
    aruco_dict:     aruco dictionary with the appropriate keys
    '''

    # possible dictionary options
    # aruco.DICT_4X4_50
    # aruco.DICT_4X4_100
    # aruco.DICT_4X4_250
    # aruco.DICT_4X4_1000
    # aruco.DICT_5X5_50
    # aruco.DICT_5X5_100
    # aruco.DICT_5X5_250
    # aruco.DICT_5X5_1000
    # aruco.DICT_6X6_50
    # aruco.DICT_6X6_100
    # aruco.DICT_6X6_250
    # aruco.DICT_6X6_1000
    # aruco.DICT_7X7_50
    # aruco.DICT_7X7_100
    # aruco.DICT_7X7_250
    # aruco.DICT_7X7_1000
    # aruco.DICT_ARUCO_ORIGINAL
    # aruco.DICT_APRILTAG_16h5
    # aruco.DICT_APRILTAG_25h9
    # aruco.DICT_APRILTAG_36h10
    # aruco.DICT_APRILTAG_36h11

    # NOTE using a dictionary which is smaller = faster searching
    aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_6X6_1000)
    aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_ARUCO_ORIGINAL)

    # if there are only select keys to be searched for then only use these
    if selectKeys is not None:
        '''
        arucoDictMod = aruco.getPredefinedDictionary(aruco.DICT_ARUCO_ORIGINAL)
        arucoDictMod.bytesList *= 0 
        for s in selectKeys:
            arucoDictMod.bytesList[int(s)] = aruco_dict.bytesList[int(s)]
        '''

        aruco_dict.bytesList = np.array([aruco_dict.bytesList[int(s)] for s in sorted(np.array(selectKeys).astype(int))])

    return(aruco_dict)

@ray.remote
def paramAssessor(imgs, paramKeys, paramValues, markerIDs, qs = None):

    '''
    Evaluate how many aruco markers are identified for a given parameter 
    dictionary and how long it takes per image

        Inputs:
    imgs:           list of image paths
    paramKeys:      list of the parameters to be modified
    paramValues:    values corresponding with the paramKeys
    markerIDs:      the aruco key values to identify on the board
    qs:             que to get info out of process parallelisation

        Outputs:
    markerCount:    number of times each markerID is counted across all images
    '''

    # create the parameter dictionary
    param = paramCreate(paramKeys, paramValues, markerIDs)

    # for every image, identify the markers
    start = time()
    for i in imgs:
        if type(i) is str:
            img = cv2.imread(i)     # NOTE for timing this includes reading in the image...
        else:
            img = i
        try:
            _, ids, _ = aruco.detectMarkers(img, createArucoDictionary(markerIDs), parameters = param.paramDict)  
            param.results(ids)
        except:
            print(f"{paramValues} not computable\n")
            break

    param.time = (time() - start)/len(imgs)
    param.paramDict = None      # this is so the object can be pickled therefore    
                                # can be passed through a parallelised workflow. It 
                                # is easily reconstructed outside of this by calling
                                # param.createParameters()
    return(param)

def optimiseParameters(imgs, paramValues, paramKeys, markerIDs, frameTime = np.inf):

    '''
    Identifies the parameters which optimise for aruco marker detection
    using ray parallelsiation

        Inputs:
    imgs:           list of numpy images
    paramValues:    parameters to assess
    paramKeys:      parameter name in aruco dictionary which corresponds with values
    markerIDs:      target arucoIDs being identified
    frameTime:      maximum time of processing per frame to accept for any parameter comnbination
    '''

    runRay = []
    startRay = time()
    paramFailedImgPut = ray.put(imgs)     # put images into shared memory
    for paramValue in paramValues: 
        runRay.append(paramAssessor.remote(paramFailedImgPut, paramKeys, paramValue, markerIDs))

    results = ray.get(runRay)
    endRay = time() - startRay
    print(f"Ray time = {np.round(endRay, 2)}")
    paramResults = []
    # enforce time constraints on parameter combinations
    for result in results:
        if result.time < frameTime:
            paramResults.append(result)

    return(paramResults)

def paramTuner(imgs, paramOld, params, markerIDs, frameTime = np.inf, parallelise = True, quick = True):

    '''
    This function takes in a list of images with aruco markers and from these, 
    the parameters are optimised to maximise the number of markerIDs identified 
    given timing constraints. 

    We are optimising to identify aruco markers in as many frames as 
    possible. 
    The time it takes to process per image is a requirement
    Identifying more aruco markers in a single frame is a secondary priority

    NOTE the optimisation is performed only on a subset of the successfully and unsuccessfully 
    identified images originally assess to save compute time.

        Inputs:
    imgs:           list of image paths
    paramOld:       the original parameter properties
    params:         the dictionary of parameters which are to be assessed in the optimisation
                        and the min/max to optimise in
    markerIDs:      a list of aruco marker values to be identified
    frameTime:      maximum time (sec) on average for the list of images for any given 
    combination of parameters
    parallelise:    boolean whether to parallelise. NOTE if set to False, ensure that the 
                        ray.init() call and ray.remote decorator are commented out.
    quick:          boolean. If False then all the images are used for the optimsation. If
                        True then a subset of the images are used. NOTE when set to True,
                        this will take a VERY long time and from my observations, no
                        improvement in results

        Outputs:
    paramsOpt: parameter object with the optimal magnitude values
    '''

    # identify in which situations the current parameters are failing
    paramSucceedImg = []
    paramFailedImg = []
    for i in imgs:
        img = cv2.imread(i)     # NOTE for timing this includes reading in the image...
        try:
            _, ids, _ = aruco.detectMarkers(img, createArucoDictionary(markerIDs), parameters = paramOld)  
            if ids is None:
                paramFailedImg.append(img)
            else:
                paramSucceedImg.append(img)
        except:
            pass

    # if the current parameters result in no failure then keep using
    # these parameters
    if len(paramFailedImg) == 0:
        print("Current params identify all images, no optimisation neederd")
        return(paramCreate(paramDict = paramOrig))
    else:
        print(f"Current params mis-identified = {len(paramFailedImg)}. Optimising...")

    if quick:
        # collect a random subset of the images
        np.random.shuffle(paramSucceedImg)              
        paramSucceedImgSelect = paramSucceedImg[:50]

        np.random.shuffle(paramFailedImg)              
        paramFailedImgSelect = paramFailedImg[:50]
    else:
        # use all the images
        paramSucceedImgSelect = paramSucceedImg
        paramFailedImgSelect = paramFailedImg

    # get the list of parameters to optimise for
    paramKeys = list(params.keys())

    # for all values and ranges, create combinations
    vAll = []
    for cV, r in list(params.values()):
        # if optimisation flag is specified, apply default range to optimise over
        # (0 --> 2 times the default value)
        if r == "o":
            r = cV * np.array([0, 0.5, 1, 2])
        if type(r) is not np.ndarray:
            r = [r]
        vAll.append(r)

    # create a list of all possible combinations 
    paramAllValues = list(product(*vAll))
    print(f"Evaluating {len(paramAllValues)} combinations")
    
    # optimise parameters using multiprocessing toolbox
    paramResults = []
    if parallelise:            
        paramResults = optimiseParameters(paramFailedImgSelect, paramAllValues, paramKeys, markerIDs, frameTime = frameTime)
    
    else:
        startSerial = time()
        for paramValue in paramAllValues: 
            paramResult = paramAssessor(paramFailedImgSelect, paramKeys, paramValue, markerIDs)
            paramResults.append(paramResult)
        endSerial = time() - startSerial
        print(f"Serial time = {np.round(endSerial, 2)}")

    # identify 5 set of parameters which identify the most frames which were not observed
    # by the previous settings
    frameSumsFail = [p.markerCount.get("imgCount", 0) for p in paramResults]
    # frameSumsFail = [p.markerCount["other"] for p in paramResults]
    
    # get all the possible parameters for a given frameSum result with a threshold of 10
    paramSumThr, paramSum = np.unique(-np.array(frameSumsFail), return_counts = True)
    paramSumSelect = -paramSumThr[np.cumsum(np.insert(paramSum[:-1], 0, 0))<10]     # alwyas select at least one value to use.
    paramSelect = np.hstack(np.where(p == frameSumsFail) for p in paramSumSelect)[0]
    paramFailure = np.array(paramResults)[paramSelect]
    paramFailValues = [p.values for p in paramFailure]
    
    # from the parameters which identify the most, previously, un-identified 
    # images, select the parameter set which identifies the most markers
    # across ALL images
    paramSuccess = optimiseParameters(paramSucceedImgSelect, paramFailValues, paramKeys, markerIDs, frameTime = frameTime)
    frameSumsSuccess = [pS.markerCount.get("imgCount", 0) + pF.markerCount.get("imgCount", 0) for pS, pF in zip(paramSuccess, paramFailure)]
    # frameSumsSuccess = [np.sum(list(pS.markerCount.values())[:-2]) + np.sum(list(pF.markerCount.values())[:-2]) for pS, pF in zip(paramSuccess, paramFailure)]
    
    maxFrameMatchesAll = np.array(paramSuccess)[np.where(np.max(frameSumsSuccess) == frameSumsSuccess)]
    markerSums = [np.sum(list(maxFromMatch.markerCount.values())[:-1]) for maxFromMatch in maxFrameMatchesAll]
    maxMarkerMatches = maxFrameMatchesAll[np.where(np.max(markerSums) == markerSums)[0]]
    
    # NOTE Create a funciton which analyses the best marker parameters to understand
    # what influences their results

    # if there are multiple parameter combinations with the same number of total
    # identified markers then choose the parameters with the fastest time
    markerTime = [maxMarkerMatche.time for maxMarkerMatche in maxMarkerMatches]
    optimumParam = maxMarkerMatches[np.argmin(markerTime)]

    # re-create the aruco dictinoary within the object
    optimumParam.paramDict = optimumParam.createParameters()

    return(optimumParam)

def saveParamDict(params, dest):

    '''
    Take an aruco detector parameter class and save it along with its
    parameters in a csv format

        Inputs:
    params:     Aruco param detector class
    dest:       destination path of the info

        Outputs:
    ():         Save the aruco params at the dest 
    '''

    paramMethods = dir(params)

    keys = np.where(np.array([d.find("_") for d in paramMethods]) == -1)[0]

    parameters = [paramMethods[k] for k in keys]

    # for some reason list comprehension doesn't work???
    values = []
    for p in parameters:
        values.append(eval(f"params.{p}"))

    with open(dest, "w") as f:
        for p, v in zip(parameters, values):
            f.write(f"{p},{v}\n")
    f.close()

def readParamDict(paramPath):

    '''
    Read in a csv file and create a acuro detector parameter class
    which has the same saved properties
    
        Inputs: 
    paramPath:      path to the saved parameter info file

        Outputs:
    paramClass:     parameter class with the saved value
    
    '''

    paramClass = aruco.DetectorParameters_create()
    paramInfo = open(paramPath, "r").readlines()
    for p in paramInfo:
        param, v = p.split(",")
        v = v.replace("\n", "").replace(" ", "")
        paramClass = paramCreate([param], [v], paramDict = paramClass).paramDict

    return(paramClass)

def compareParameters(imgs ,arucoIDs, paramOrig, paramOptimised, visualise = True):

    '''
    Comparing the original parameters to the optimsed ones for the entire data set.

        Inputs
    imgs:               list of image paths
    arucoIDs:           the aruco ids being specified (either None or a list of id numbers)
    paramOrig:          the original aruco parameters to compare againist
    paramOptimised:     the optimised parameters
    visualse:           boolean. If true will show the detected markers for the optimised and orignal params

        Outputs
    ()                  The counts of the individual occurences of each id. 
                        If visualise, then the actual detected markers
    '''

    # count the occurence of each key
    valueStore = {}

    # create parameter objects to store results
    paramM = paramCreate(markerIDs = arucoIDs)
    paramO = paramCreate(markerIDs = arucoIDs)

    arucoDict = createArucoDictionary(arucoIDs)

    # evaluate the results of the new and old aruco marker parameters
    for n, i in enumerate(imgs):
        printProgressBar(n, len(imgs)-1, "Imgs", length=20)
        img = cv2.imread(i)     
        try:
            corners, ids, _ = aruco.detectMarkers(img, arucoDict, parameters = paramOptimised)  
            cornersOrig, idsOrig, _ = aruco.detectMarkers(img, arucoDict, parameters = paramOrig)  

            if visualise: # (len(corners) != len(cornersOrig) or len(corners) == 0):
                imgModMarked = aruco.drawDetectedMarkers(img.copy(), corners, ids, [0, 0, 255])
                imgOrigMarked = aruco.drawDetectedMarkers(img.copy(), cornersOrig, idsOrig, [0, 0, 255])
                cv2.imshow(f"imgMod, imgModORIG", np.hstack([imgModMarked, imgOrigMarked])); cv2.waitKey(1)
            # store info about how many of each id have been identified
            if ids is not None:
                paramM.results(ids)
                paramO.results(idsOrig)

        except:
            print(f"not computable", end = "")
            break
    
    # get the results of the comparsions
    paramModDict = paramM.markerCount
    paramOrigDict = paramO.markerCount

    modTotal = 0
    origTotal = 0
    for (id, pM, pO) in zip(paramModDict.keys(), paramModDict.values(), paramOrigDict.values()):
        print(f"{id}: mod = {pM}, orig = {pO}")
        modTotal += pM
        origTotal += pO
    print(f"Total: mod = {modTotal - paramModDict['imgCount']}, orig = {origTotal - paramOrigDict['imgCount']}")

if __name__ == "__main__":

    # get image data
    imgsrc = "/Volumes/USB/blue_aruco_data2mod/"
    imgsrc = "blue_aruco_dataSmall/"
    imgsrc = "/Volumes/USB/dark_aruco_data/"
    imgsrc = "dark_aruco_dataSmall/"

    imgs = sorted(glob(imgsrc + "*.jpg"))

    # specify the parameter, the orignal value and the range of values to assess 
    # for optimisation
    paramOrig = aruco.DetectorParameters_create()
    paramOrig.minDistanceToBorder = 0       # this can be hardcoded

    # "{Parameter}": [paramOrig.{Parameter}, {control}]
    # contorl is one of the follwoing: 
    #   a single specified value
    #   a range of values (this will be iterated over for every combination during the optimisation)  
    #   None (uses default value)
    #   "o" which will apply a default range of 0, 0.5, 1 and 2x the default value
    paramOptions = {
    "minDistanceToBorder": [paramOrig.minDistanceToBorder, 0], 
    "adaptiveThreshWinSizeStep": [paramOrig.adaptiveThreshWinSizeStep, 1],
    "adaptiveThreshWinSizeMax": [paramOrig.adaptiveThreshWinSizeMax, np.linspace(60, 120, 4)],
    "adaptiveThreshConstant": [paramOrig.adaptiveThreshConstant, 2],
    # "polygonalApproxAccuracyRate": [paramOrig.polygonalApproxAccuracyRate, np.linspace(0.05, 0.1, 6)],
    # "perspectiveRemovePixelPerCell": [paramOrig.perspectiveRemovePixelPerCell, np.linspace(1, 7, 4)]

    # all other parameters which are NOT being modified during optimisation
    "adaptiveThreshWinSizeMin" : [paramOrig.adaptiveThreshWinSizeMin, None],
    "aprilTagCriticalRad" : [paramOrig.aprilTagCriticalRad, None],
    "aprilTagDeglitch" : [paramOrig.aprilTagDeglitch, None],
    "aprilTagMaxLineFitMse" : [paramOrig.aprilTagMaxLineFitMse, None],
    "aprilTagMaxNmaxima" : [paramOrig.aprilTagMaxNmaxima, None],
    "aprilTagMinClusterPixels" : [paramOrig.aprilTagMinClusterPixels, None],
    "aprilTagMinWhiteBlackDiff" : [paramOrig.aprilTagMinWhiteBlackDiff, None],
    "aprilTagQuadDecimate" : [paramOrig.aprilTagQuadDecimate, None],
    "aprilTagQuadSigma" : [paramOrig.aprilTagQuadSigma, None],
    "cornerRefinementMaxIterations" : [paramOrig.cornerRefinementMaxIterations, None],
    "cornerRefinementMethod" : [paramOrig.cornerRefinementMethod, None],
    "cornerRefinementMinAccuracy" : [paramOrig.cornerRefinementMinAccuracy, None],
    "cornerRefinementWinSize" : [paramOrig.cornerRefinementWinSize, None],
    "detectInvertedMarker" : [paramOrig.detectInvertedMarker, None],
    "errorCorrectionRate" : [paramOrig.errorCorrectionRate, None],
    "markerBorderBits" : [paramOrig.markerBorderBits, None],
    "maxErroneousBitsInBorderRate" : [paramOrig.maxErroneousBitsInBorderRate, None],
    "maxMarkerPerimeterRate" : [paramOrig.maxMarkerPerimeterRate, None],
    "minCornerDistanceRate" : [paramOrig.minCornerDistanceRate, None],
    "minMarkerDistanceRate" : [paramOrig.minMarkerDistanceRate, None],
    "minMarkerPerimeterRate" : [paramOrig.minMarkerPerimeterRate, None],
    "minOtsuStdDev" : [paramOrig.minOtsuStdDev, None],
    "perspectiveRemoveIgnoredMarginPerCell" : [paramOrig.perspectiveRemoveIgnoredMarginPerCell, None],
    "perspectiveRemovePixelPerCell" : [paramOrig.perspectiveRemovePixelPerCell, None],
    "polygonalApproxAccuracyRate" : [paramOrig.polygonalApproxAccuracyRate, None],
    }

    # specify the marker IDs being searched for
    arucoIDs = None # ["4", "114", "164", "371", "638", "903"]
    # arucoIDs = [447, 322, 445, 2, 168, 173, 325, 455, 120, 417, 779, 68]

    # get the optimal parameters for aruco marker identificaiton
    # Set this to False to NOT optimise and only assess
    if True:
        paramTunerResults = paramTuner(imgs, paramOrig, paramOptions, arucoIDs)
        paramOptimised = paramTunerResults.paramDict
        saveParamDict(paramOptimised, "paramOptimisedDark2.csv")
    else:
        paramOptimised = readParamDict("paramOptimisedDark2.csv")

    # visually assess the effect of the optimised vs original parameters
    compareParameters(imgs ,arucoIDs, paramOrig, paramOptimised, visualise = True)