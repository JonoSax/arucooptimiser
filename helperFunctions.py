from time import time
import numpy as np
import cv2.aruco as aruco
from glob import glob
import cv2


def testingDownSampling():
    '''
    Testing the variations of time based on the downsampling of the images.
    It appears there is almost minimal decrase in time...
        '''
    imgsrc = "arucoScaleTestData/"
    imgs = sorted(glob(imgsrc + "*.jpg"))

    #  multi-scale image search
    times = {}
    for n in range(1,10):
        start = time()
        for i in imgs * 1:
            img = cv2.imread(i)
            imgResize = cv2.resize(img, (int(img.shape[1]/n), int(img.shape[0]/n)))
            aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_ARUCO_ORIGINAL)
            corners, ids, rejectedImgPoints = aruco.detectMarkers(imgResize, aruco_dict)
        end = time() - start
        times[n] = end
        print("Downsample factor %i = %.2f sec" % (n, end))

def testingTanandBool():

    '''
    Testing if tan or bool operations are faster, In python tanh is....
    '''

    testImg = np.random.random([400, 600])
    startTan = time()
    for i in range(1000):
        img = np.tanh(testImg)
    finTan = time() - startTan

    startBool = time()
    for i in range(1000):
        img = testImg > 0.5
    finBool = time() - startTan

    print("%f.2 tan time, %f.2 bool time" % (np.round(finTan, 2), np.round(finBool)))

def downsample():

    imgsrc = "arucoScaleTestData3/"
    imgdest = "arucoScaleTestdata3Small/"
    imgs = sorted(glob(imgsrc + "*.jpg"))

    scl = 0.2

    for n, i in enumerate(imgs):
        name = str(n)
        while len(name) < 3:
            name = "0" + name
        img = cv2.imread(i)

        imgN = cv2.resize(img, (int(img.shape[1]*scl), int(img.shape[0]*scl)))
        cv2.imwrite(f"{imgdest}/{name}.jpg", imgN)
        print(f"{n}/{len(imgs)}")

def rayTest():

    import ray
    import time

    # Start Ray.
    ray.init()

    @ray.remote
    def f(x):
        time.sleep(0)
        return x

    # Start 4 tasks in parallel.
    result_ids = []
    for i in range(4):
        result_ids.append(f.remote(i))
        
    # Wait for the tasks to complete and retrieve the results.
    # With at least 4 cores, this will take 1 second.
    results = ray.get(result_ids)  # [0, 1, 2, 3]

    print(results)

def extractFramesFromVideo():

    '''
    Convert a video into images
    '''

    vidsrc = "arucoScaleTestData3.MOV"
    framedest = "arucoScaleTestData3"

    frameCapt = 5
    cap = cv2.VideoCapture(vidsrc)
    n = 0

    # Check if camera opened successfully
    if (cap.isOpened()== False): 
        print("Error opening video stream or file")

    # Read until video is completed
    while(cap.isOpened()):
        
        # Capture frame-by-frame
        ret, frame = cap.read()
        if ret == False:
            break
        elif n%frameCapt == 0:
            name = str(n)
            while len(name) < 4:
                name = "0" + name
            cv2.imwrite(f"{framedest}/{name}.jpg", frame)

        n += 1

    # When everything done, release the video capture object
    cap.release()

    # Closes all the frames
    cv2.destroyAllWindows()

if __name__ == "__main__":

    extractFramesFromVideo()
    downsample()
