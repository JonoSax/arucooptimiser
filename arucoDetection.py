'''
this script is implementing as many of the aruco functions as possible
to just understand what it is doing.

NOTE this needs to be translated into C++
'''

from time import time
import cv2.aruco as aruco
from glob import glob
import cv2
import numpy as np
from matplotlib import pyplot as plt
import json

def createArucoDictionary():

    '''
    Creating an dictionary which specified the aruco settings 
    which can be used to determine the aruco processes

    Choose the default dictionary from the list below (as taken 
    from https://docs.opencv.org/master/d9/d6a/group__aruco.html#gac84398a9ed9dd01306592dd616c2c975)
    or create a custom dictionary 

    NOTE create layout for custom dictionary
    '''

    # possible dictionary options
    # aruco.DICT_4X4_50
    # aruco.DICT_4X4_100
    # aruco.DICT_4X4_250
    # aruco.DICT_4X4_1000
    # aruco.DICT_5X5_50
    # aruco.DICT_5X5_100
    # aruco.DICT_5X5_250
    # aruco.DICT_5X5_1000
    # aruco.DICT_6X6_50
    # aruco.DICT_6X6_100
    # aruco.DICT_6X6_250
    # aruco.DICT_6X6_1000
    # aruco.DICT_7X7_50
    # aruco.DICT_7X7_100
    # aruco.DICT_7X7_250
    # aruco.DICT_7X7_1000
    # aruco.DICT_ARUCO_ORIGINAL
    # aruco.DICT_APRILTAG_16h5
    # aruco.DICT_APRILTAG_25h9
    # aruco.DICT_APRILTAG_36h10
    # aruco.DICT_APRILTAG_36h11

    # NOTE using a dictionary which is smaller = faster searching
    aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_6X6_1000)
    aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_ARUCO_ORIGINAL)


    return(aruco_dict)

def drawArucoPoints(img, info, scl = 1, col = [0, 0, 255]):

    '''
    Take an aruco board and draw the information extracted 

    NOTE aruco vs charcuo: https://docs.opencv.org/3.4/df/d4a/tutorial_charuco_detection.html

        Inputs:
    img: numpy array of the aruco board
    info: dictionary of the acuro boundaries

        Outputs:
    imgMod: modified image with the acuro information annotated
    '''

    imgMod = img.copy()

    if len(imgMod.shape) == 2:
        imgMod = np.stack((imgMod,)*3, axis=-1)

    # custom annotaiton of image 
    for cS, i in zip(info.values(), info.keys()):

        # draw the target points
        for c in cS:
            cv2.circle(imgMod, c.astype(int), 4*scl, col, 3*scl)

        # put the marker ID in the annotations
        cv2.putText(imgMod, str(i), np.mean(cS, axis = 0).astype(int), cv2.FONT_HERSHEY_SIMPLEX, 0.5*scl, [0, 0, 0], 2*scl)
    
    # draw identified but rejected points
    '''
    for rs in rejectedImgPoints:
        for r in rs[0]:
            cv2.circle(imgMod, r.astype(int), 4, [255, 0, 0], 3)
    '''

    # in built marker annotation
    # imgMod2 = aruco.drawDetectedMarkers(imgMod2, corners, ids, [0, 0, 255])

    return(imgMod)

def detectCharucoPoints(img, arucoDict):

    '''
    Take an charuco board and draw the information extracted 

    NOTE aruco vs charcuo: https://docs.opencv.org/3.4/df/d4a/tutorial_charuco_detection.html

        Inputs:
    img: numpy array of the aruco board
    arucoDict: dictionary of the acuro properties

        Outputs:
    imgMod: modified image with the acuro information annotated
    '''


    pass

def detectArucoPoints(img, arucoDict, param, scale = 1):

    '''
    Get the images and identify all the aruco markers for a range of image intensities

        Input:
    img: numpy array of the image
    arucoDict: dictionary of the aruco values
    scale: optional to downsample images

        Output:
    arucoCorners: dictionary containing the aruco points and their ID
    '''


    # iterate through multiple background intensities to isolate each aruco
    # point 

    if scale != 1:
        img = cv2.resize(img, (int(img.shape[1]*scale), int(img.shape[0]*scale)))
    
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)     # Gray-scale

    refine_cornersAll = {}

    # if there is no range defined, just perform basic aruco detection
    _, _, refine_cornersAll = findAruceMarkers(img, arucoDict, refine_cornersAll, param)

    # if multi - thresholding is specified
    if type(param) is type(np.array):
        imgModAll = []
        imgModFail = []
        for thr in param:

            # perform thresholding
            thrImg = np.tanh((imgGray.astype(int) - thr))+1
            imgMod = (255*(thrImg - np.min(thrImg))/(np.max(thrImg) - np.min(thrImg))).astype(np.uint8)
            imgMod = cv2.dilate(imgMod, (3, 3), iterations = 5)
            # cv2.imshow("imgThr", imgMod); cv2.waitKey(400)

            # identify points
            refine_cornersNew, rejected_corners, refine_cornersAll = findAruceMarkers(imgMod, arucoDict, refine_cornersAll)
            
            # save the processing images
            if len(refine_cornersNew) > 0:
                imgMod = drawArucoPoints(imgMod, refine_cornersNew, 2)
                imgMod = drawArucoPoints(imgMod, rejected_corners, 2, [255, 0, 0]) 
                imgModAll.append(imgMod)
            else:            
                imgModFail.append(imgMod)

        if len(imgModAll) > 0:
            cv2.imshow("imgThrSuccess", np.vstack(imgModAll)); cv2.waitKey(1)
        cv2.imshow("ImgThrFail", np.vstack(imgModFail)); cv2.waitKey(1)

    # from all the positions found across all intensities, identify the most likely 
    # corner points
    arucoCorners = {}
    for rP, rI in zip(refine_cornersAll.values(), refine_cornersAll.keys()):
        arucoCorners[rI] = np.median(rP, axis = 0)

    return(arucoCorners)

def findAruceMarkers(img, arucoDict, refine_cornersAll, paramR = None):

    '''
    Take an image and perform aruco detection and save the detected points
    in a dictionary

        Inputs:
    img: image which may or may not contain an aruco marker/s
    arucoDict: dictionary used to identify the aruco marker IDs
    refine_cornersAll: the dictionary which is update to store all the 
    aruco corner positions

    NOTE params: https://docs.opencv.org/master/d1/dcd/structcv_1_1aruco_1_1DetectorParameters.html#aef7291f7771315af9726eafacb7fc955
    param.adaptiveThreshWinSizeMin                  # (default 3)
    param.adaptiveThreshWinSizeMax                  # (default 23)
    param.adaptiveThreshWinSizeStep                 # (default 10)
    param.adaptiveThreshConstant                    # (default 7)
    param.minMarkerPerimeterRate                    # (default 0.03)
    param.maxMarkerPerimeterRate                    # (default 4.0)
    param.polygonalApproxAccuracyRate               # (default 0.03)
    param.minCornerDistanceRate                     # (default 0.05)
    param.minDistanceToBorder                       # (default 3)
    param.cornerRefinementMethod                    # (default CORNER_REFINE_NONE)
    param.cornerRefinementWinSize                   # (default 5)
    param.cornerRefinementMaxIterations             # (default 30)
    param.cornerRefinementMinAccuracy               # (default: 0.1)
    param.markerBorderBits                          # (default 1)
    param.perspectiveRemovePixelPerCell             # (default 4)
    param.perspectiveRemoveIgnoredMarginPerCell     # (default 0.13)
    param.maxErroneousBitsInBorderRate              # (default 0.35)
    param.minOtsuStdDev                             # (default 5.0)
    param.errorCorrectionRate                       # (default 0.6)
    param.aprilTagMinClusterPixels                  # (default 5)
    param.aprilTagMaxNmaxima                        # (default 10)
    param.aprilTagCriticalRad                       # (default 10*PI/180)
    param.aprilTagMaxLineFitMse                     # (default 10.0)
    param.aprilTagMinWhiteBlackDiff                 # (default 5)
    param.aprilTagDeglitch                          # (default 0)
    param.aprilTagQuadDecimate                      # (default 0.0)
    param.aprilTagQuadSigma                         # (default 0.0)
    param.detectInvertedMarker                      # (default false)
    '''

    # detect aruco board markers
    param = aruco.DetectorParameters_create()

    # parameter tuning which indicatively improves aruco detection
    param.minDistanceToBorder = 0                      

    # all the possible parameters to vary for aruo detection
    # perform a default detection
    cornersStore, idsStore, rejectedImgPoints = aruco.detectMarkers(img, arucoDict, parameters = param)

    if paramR is not None:
        # ensure that param is iterable
        try: len(paramR)
        except: paramR = [paramR]

        for n in paramR:

            param.adaptiveThreshConstant = n

            # this helps to identify points which are on a large slant            
            corners, ids, _ = aruco.detectMarkers(img, arucoDict, parameters = param)
            imgmod = aruco.drawDetectedMarkers(img.copy(), corners, ids, [0, 0, 255])
            cv2.imshow(f"imgmod", imgmod); cv2.waitKey(0)
            # ensure that each entry is unique
            if len(corners) > 0:
                cornersStore, idsStore = getUniqueMarkers(corners, ids, cornersStore, idsStore)
        
    # id store must = None if there are no entires
    if idsStore is not None:
        if len(idsStore) == 0:
            idsStore = None

    # NOTE not sure if i need to do a refinement? TBC if it actually improves
    # the results....
    # refine the aruco board detection (find any markers which haven't already
    # been detected)
    markersX = 4                # number of markers in X direction
    markersY = 5                # number of markers in Y direction
    markerLength = 0.1        # marker side length (meters)
    markerSeparation = 0.02    # seperation between markers (meters)
    board = aruco.GridBoard_create(markersX, markersY, markerLength, markerSeparation, arucoDict)

    # NOTE this requires camera calibration which is not performed by the aruco library
    # calib = cv2.aruco.estimatePoseBoard(corners, ids, board, np.array([]))

    refine_corners, refine_ids, rejectedCorners, _ = aruco.refineDetectedMarkers(img, board, cornersStore, idsStore, rejectedImgPoints)
    # imgAnno = aruco.drawDetectedMarkers(img.copy(), refine_corners, refine_ids, [0, 0, 255])
    #Mcv2.imshow("img", img); cv2.waitKey(200)

    # convert the acruo info into a useful dictionary
    refine_cornersNew = {}
    rejectedCorners = {}
    if rejectedCorners is not None:
        for id, rj_c in enumerate(rejectedCorners):
            refine_cornersNew[id] = []
            # ensure only points with 4 corners identified are used
            if len(rj_c[0]) == 4:
                rejectedCorners[id].append(rj_c[0][0])

    if refine_ids is not None:
        for r_id, r_c in zip(refine_ids, refine_corners):
            id = r_id[0]
            refine_cornersNew[id] = []
            if refine_cornersAll.get(id) is None:
                refine_cornersAll[id] = []

            # ensure only points with 4 corners identified are used
            if len(r_c[0]) == 4:
                refine_cornersAll[id].append(r_c[0])
                refine_cornersNew[id].append(r_c[0][0])

    return(refine_cornersNew, rejectedCorners, refine_cornersAll)

def getUniqueMarkers(corners, ids, cornersStore, idsStore):

    if idsStore is None:
        idsStore = []
    # get the unique id entries
    _, idsX = np.unique([i[0] for i in list(idsStore) + list(ids)], return_index = True)

    # re-assign the variables to only contain unique entries of each id
    cornersStore = [(cornersStore + corners)[i] for i in idsX]
    idsStore = np.array([(list(idsStore) + list(ids))[i] for i in idsX])

    return(cornersStore, idsStore)

# get test images 
if __name__ == "__main__":

    # get image data
    imgsrc = "aruco_data/"
    imgsrc = "blue_aruco_dataSmall/"
    imgs = sorted(glob(imgsrc + "*.jpg"))

    param = aruco.DetectorParameters_create()
    # paramList = [list(dir(param))[n] for n in np.where(np.array([d.find("_") for d in dir(param)])<0)[0]]

    paramOptions = {
        "adaptiveThreshWinSizeMin": param.adaptiveThreshWinSizeMin,
        "adaptiveThreshWinSizeMax": param.adaptiveThreshWinSizeMax,
        "adaptiveThreshWinSizeStep": param.adaptiveThreshWinSizeStep,
        "adaptiveThreshConstant": param.adaptiveThreshConstant,
        "minMarkerPerimeterRate": param.minMarkerPerimeterRate,
        "maxMarkerPerimeterRate": param.maxMarkerPerimeterRate,
        "polygonalApproxAccuracyRate": param.polygonalApproxAccuracyRate,
        "minCornerDistanceRate": param.minCornerDistanceRate,
        "minDistanceToBorder": param.minDistanceToBorder,
        "minMarkerDistanceRate": param.minMarkerDistanceRate,
        "cornerRefinementMethod": param.cornerRefinementMethod,
        "cornerRefinementWinSize": param.cornerRefinementWinSize,
        "cornerRefinementMaxIterations": param.cornerRefinementMaxIterations,
        "cornerRefinementMinAccuracy": param.cornerRefinementMinAccuracy,
        "markerBorderBits": param.markerBorderBits,
        "perspectiveRemovePixelPerCell": param.perspectiveRemovePixelPerCell,
        "perspectiveRemoveIgnoredMarginPerCell": param.perspectiveRemoveIgnoredMarginPerCell,
        "maxErroneousBitsInBorderRate": param.maxErroneousBitsInBorderRate,
        "minOtsuStdDev": param.minOtsuStdDev,
        "errorCorrectionRate": param.errorCorrectionRate,
        "aprilTagMinClusterPixels": param.aprilTagMinClusterPixels,
        "aprilTagMaxNmaxima": param.aprilTagMaxNmaxima,
        "aprilTagCriticalRad": param.aprilTagCriticalRad,
        "aprilTagMaxLineFitMse": param.aprilTagMaxLineFitMse,
        "aprilTagMinWhiteBlackDiff": param.aprilTagMinWhiteBlackDiff,
        "aprilTagDeglitch": param.aprilTagDeglitch,
        "aprilTagQuadDecimate": param.aprilTagQuadDecimate,
        "aprilTagQuadSigma": param.aprilTagQuadSigma,
        "detectInvertedMarker": param.detectInvertedMarker
    }

    # get a default aruco dictionary
    arucoDict = createArucoDictionary()

    # automatically assess the impact which each hyper parameter has on
    # the outcome of detecting the ids

    # for each variable modify the 
    valueStore = {}
    for pK, pV in zip(paramOptions.keys(), paramOptions.values()):
        valueStore[pK] = {}
        typeV = type(pV)
        print(f"\n{pK}")
        for r in range(6):
            if type(pV) == int:
                newVal = int(pV * r/3)
                exec(f"param.{pK} = {newVal}")
            elif type(pV) == float:
                newVal = float(pV * r/3)
                exec(f"param.{pK} = {newVal}")
            else:
                print(f"{pK} not evaluated")
                continue
            valueStore[pK][newVal] = {}
            
            print(f"    {newVal}", end = " ")

            # process per image
            start = time()
            for n, i in enumerate(imgs):
                # start = time()
                # detect the aruco corners and associated key 
                imgOrig = cv2.imread(i)               

                try:
                    corners, ids, _ = aruco.detectMarkers(imgOrig, arucoDict, parameters = param)  

                    # store info about how many of each id have been identified
                    if ids is not None:
                        for i in ids:
                            i = str(i[0])
                            if valueStore[pK][newVal].get(i, None) is None:
                                valueStore[pK][newVal][i] = 0
                            valueStore[pK][newVal][i] += 1

                except:
                    valueStore[pK][newVal] = "Not computable"
                    print(f"not computable", end = "")
                    break

            end = time() - start
            try:
                valueStore[pK][newVal]["Time"] = np.round(end, 2)
            except:
                pass
                
            print("")

    a = open("valueStore.json", "w")
    json.dump(valueStore, a)
    a.close()
    print(valueStore)
                
    '''
    # if the aruco ids detected are not the same, show images
    elif False:
        for v in refine_cornersThr.keys():
            if refine_cornersNone.get(v) is None:
                draw = True
                break
            if refine_cornersDefault.get(v) is None:
                draw = True
                break
    '''
    
    '''
    imgAnnoDetectedCharucoPoints = detectCharucoPoints(img, arucoDict)
    cv2.imshow("charucoBoard", imgAnnoDetectedCharucoPoints)
    cv2.waitKey(0)
    '''
            
